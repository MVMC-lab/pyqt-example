import sys
import serial

from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import QFile, QIODevice
from PyQt5.QtGui import QIcon, QPixmap

from .ui_mainwindow import Ui_MainWindow

import app.qrc


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        self.label_imgPig.setPixmap(QPixmap(":/images/user_pig.png"))
        self.label_imgRabbit.setPixmap(QPixmap(":/images/user_rabbit.png"))

        fd = QFile(":/txts/readme.txt")

        if fd.open(QIODevice.ReadOnly):
            # 使用 member function open 開啟檔案
            # 若開啟成功就進入 if

            text = bytes(fd.readAll()).decode('utf8')
            # fd.readAll() 讀回資料型態是 QByteArray
            # 利用 bytes 轉型成 python 的 ByteArray
            # 再進行 utf8 解碼

            fd.close()
            # 關閉檔案

            self.label_readme.setText(text)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
