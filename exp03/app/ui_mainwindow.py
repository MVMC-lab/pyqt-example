# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(238, 237)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_rabbit = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_rabbit.setObjectName("pushButton_rabbit")
        self.gridLayout.addWidget(self.pushButton_rabbit, 1, 2, 1, 1)
        self.pushButton_pig = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_pig.setObjectName("pushButton_pig")
        self.gridLayout.addWidget(self.pushButton_pig, 1, 1, 1, 1)
        self.label_imgRabbit = QtWidgets.QLabel(self.centralwidget)
        self.label_imgRabbit.setText("")
        self.label_imgRabbit.setObjectName("label_imgRabbit")
        self.gridLayout.addWidget(self.label_imgRabbit, 0, 2, 1, 1)
        self.label_imgPig = QtWidgets.QLabel(self.centralwidget)
        self.label_imgPig.setText("")
        self.label_imgPig.setObjectName("label_imgPig")
        self.gridLayout.addWidget(self.label_imgPig, 0, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.label_readme = QtWidgets.QLabel(self.centralwidget)
        self.label_readme.setText("")
        self.label_readme.setObjectName("label_readme")
        self.verticalLayout.addWidget(self.label_readme)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 238, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_rabbit.setText(_translate("MainWindow", "rabbit"))
        self.pushButton_pig.setText(_translate("MainWindow", "pig"))

