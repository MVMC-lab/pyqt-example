# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/widget_b.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WidgetB(object):
    def setupUi(self, WidgetB):
        WidgetB.setObjectName("WidgetB")
        WidgetB.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(WidgetB)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(WidgetB)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.lineEdit = QtWidgets.QLineEdit(WidgetB)
        self.lineEdit.setObjectName("lineEdit")
        self.verticalLayout.addWidget(self.lineEdit)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_1 = QtWidgets.QPushButton(WidgetB)
        self.pushButton_1.setObjectName("pushButton_1")
        self.horizontalLayout.addWidget(self.pushButton_1)
        self.pushButton_2 = QtWidgets.QPushButton(WidgetB)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(WidgetB)
        QtCore.QMetaObject.connectSlotsByName(WidgetB)

    def retranslateUi(self, WidgetB):
        _translate = QtCore.QCoreApplication.translate
        WidgetB.setWindowTitle(_translate("WidgetB", "Form"))
        self.label.setText(_translate("WidgetB", "我是 WidgetB"))
        self.pushButton_1.setText(_translate("WidgetB", "pushButton_1"))
        self.pushButton_2.setText(_translate("WidgetB", "pushButton_2"))

