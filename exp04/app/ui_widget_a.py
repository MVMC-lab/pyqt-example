# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/widget_a.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WidgetA(object):
    def setupUi(self, WidgetA):
        WidgetA.setObjectName("WidgetA")
        WidgetA.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(WidgetA)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(WidgetA)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.textBrowser = QtWidgets.QTextBrowser(WidgetA)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_1 = QtWidgets.QPushButton(WidgetA)
        self.pushButton_1.setObjectName("pushButton_1")
        self.horizontalLayout.addWidget(self.pushButton_1)
        self.pushButton_2 = QtWidgets.QPushButton(WidgetA)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(WidgetA)
        QtCore.QMetaObject.connectSlotsByName(WidgetA)

    def retranslateUi(self, WidgetA):
        _translate = QtCore.QCoreApplication.translate
        WidgetA.setWindowTitle(_translate("WidgetA", "Form"))
        self.label.setText(_translate("WidgetA", "我是 WidgetA"))
        self.pushButton_1.setText(_translate("WidgetA", "pushButton_1"))
        self.pushButton_2.setText(_translate("WidgetA", "pushButton_2"))

