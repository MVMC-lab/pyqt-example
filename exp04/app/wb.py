from .ui_widget_b import Ui_WidgetB


class WidgetB_Handler(Ui_WidgetB):
    def __init__(self, widget):
        super(WidgetB_Handler, self).__init__()
        self.setupUi(widget)
        self.pushButton_1.clicked.connect(self.func1)
        self.pushButton_2.clicked.connect(self.func2)

    def func1(self):
        self.lineEdit.setText('pushButton_1')

    def func2(self):
        self.lineEdit.setText('pushButton_2')
