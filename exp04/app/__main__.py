import sys

from PyQt5.QtWidgets import QMainWindow, QApplication

from .ui_mainwindow import Ui_MainWindow

from .wa import WidgetA_Handler
from .wb import WidgetB_Handler


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.w1 = WidgetA_Handler(self.widget_1)
        self.w2 = WidgetA_Handler(self.widget_2)
        self.w3 = WidgetB_Handler(self.widget_3)
        self.w4 = WidgetB_Handler(self.widget_4)

        self.widget_2.hide()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
