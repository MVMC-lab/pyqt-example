from .ui_widget_a import Ui_WidgetA


class WidgetA_Handler(Ui_WidgetA):
    def __init__(self, widget):
        super(WidgetA_Handler, self).__init__()
        self.setupUi(widget)
        self.pushButton_1.clicked.connect(self.func1)
        self.pushButton_2.clicked.connect(self.func2)

    def func1(self):
        self.textBrowser.append('btn1 clicked')

    def func2(self):
        self.textBrowser.append('btn2 clicked')
