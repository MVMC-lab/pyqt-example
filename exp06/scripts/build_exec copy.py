import PyInstaller.__main__

import shutil

# NOTE you need to change the path if you want to build it.
# A path to search for imports (like using PYTHONPATH).
# If it has been include in yout system PATH, you don't have to add it.
PATH = [
    # NOTE Need libs in package PyQt5.
    'D:/Programs/Python/Python36-32/Lib/site-packages/PyQt5/Qt/bin',
    'D:/Programs/Python/Python36-32/Lib/site-packages/PyQt5/Qt/plugins',

    # NOTE Need Windows Kits 10 to support win10.
    'C:/Program Files (x86)/Windows Kits/10/Redist/ucrt/DLLs/x86',
    'C:/Program Files (x86)/Windows Kits/10/Redist/ucrt/DLLs/x64',

    # NOTE Need libs in package numpy and scipy.
    'D:/Programs/Python/Python36-32/Lib/site-packages/scipy/extra-dll',
    'D:/Programs/Python/Python36-32/Lib/site-packages/numpy/.libs',
    'D:/Programs/Python/Python36-32/Lib/site-packages/zmq',
    '.'
]

HIDEEN_IMPORT = [
    'PyQt5.sip'
]

def build_exec():
    opts = [
        'scripts/entrypoint.py'
        ]
    opts += ['--noupx', '--clean']

    # NOTE '--onefile' Create a one-file bundled executable.
    # maybe not stable in some environment, need more test.
    opts += ['--onefile']

    # NOTE if you want exe not to print info to I/O, use '-w' .
    # opts += ['-w']

    # NOTE if ypu want to extra debug info, use the '-debug'.
    # opts += ['--debug']

    opts += [f'--path={path}' for path in PATH]
    opts += [f'--hidden-import={path}' for path in HIDEEN_IMPORT]
    # print(opts)
    PyInstaller.__main__.run(opts)


def run():
    build_exec()


if __name__ == '__main__':
    run()

