from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import pyqtSignal

from .ui_dialog_a import Ui_DialogA


class DialogA(QDialog, Ui_DialogA):
    sig_msgAccepted = pyqtSignal(str)

    def __init__(self, parent=None):
        super(DialogA, self).__init__(parent)
        self.setupUi(self)

        self.pushButton_ok.clicked.connect(self.accept)
        self.pushButton_cancel.clicked.connect(self.reject)

    def accept(self):
        text = self.lineEdit.text()
        self.sig_msgAccepted.emit(text)
        super(DialogA, self).accept()
