# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/dialog_a.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogA(object):
    def setupUi(self, DialogA):
        DialogA.setObjectName("DialogA")
        DialogA.resize(302, 93)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogA)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(DialogA)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.lineEdit = QtWidgets.QLineEdit(DialogA)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 0, 0, 1, 2)
        self.pushButton_ok = QtWidgets.QPushButton(DialogA)
        self.pushButton_ok.setObjectName("pushButton_ok")
        self.gridLayout.addWidget(self.pushButton_ok, 1, 1, 1, 1)
        self.pushButton_cancel = QtWidgets.QPushButton(DialogA)
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.gridLayout.addWidget(self.pushButton_cancel, 1, 2, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)

        self.retranslateUi(DialogA)
        QtCore.QMetaObject.connectSlotsByName(DialogA)

    def retranslateUi(self, DialogA):
        _translate = QtCore.QCoreApplication.translate
        DialogA.setWindowTitle(_translate("DialogA", "Dialog"))
        self.label.setText(_translate("DialogA", "請輸入任何訊息"))
        self.pushButton_ok.setText(_translate("DialogA", "OK"))
        self.pushButton_cancel.setText(_translate("DialogA", "Cancel"))

