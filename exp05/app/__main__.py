import sys
import serial

from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog

from .ui_mainwindow import Ui_MainWindow

from .dialog_a import DialogA


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.pushButton_1.clicked.connect(self.func1)
        self.pushButton_2.clicked.connect(self.func2)
        self.dialogA = DialogA()

        self.dialogA.accepted.connect(
            lambda: self.textBrowser.append('accepted!'))
        self.dialogA.rejected.connect(
            lambda: self.textBrowser.append('rejected!'))
        self.dialogA.sig_msgAccepted.connect(self.textBrowser.append)

    def func1(self):
        res = self.dialogA.exec()
        print(res)

    def func2(self):
        fname, ftype = QFileDialog.getOpenFileName(
            self,
            caption='Open file',
            directory='c:\\',
            filter="All Files (*);;txt Files (*.txt)",
            initialFilter='txt Files (*.txt)'
        )
        print(f'fname is {fname}')
        print(f'ftype is {ftype}')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
